package ru.yandex.testtask.support;

import ru.yandex.testtask.controllers.HttpDownloadController;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * Created by kalasd on 6/23/2014.
 */
public class SupportTestUtils {
    public static boolean isFileBinaryEqual(
            File first,
            File second
    ) throws IOException {
        boolean retval = false;

        if ((first.exists()) && (second.exists())
                && (first.isFile()) && (second.isFile())) {
            if (first.getCanonicalPath().equals(second.getCanonicalPath())) {
                retval = true;
            } else {
                FileInputStream firstInput = null;
                FileInputStream secondInput = null;
                BufferedInputStream bufFirstInput = null;
                BufferedInputStream bufSecondInput = null;

                try {
                    firstInput = new FileInputStream(first);
                    secondInput = new FileInputStream(second);
                    bufFirstInput = new BufferedInputStream(firstInput, 65536);
                    bufSecondInput = new BufferedInputStream(secondInput, 65536);

                    int firstByte;
                    int secondByte;

                    while (true) {
                        firstByte = bufFirstInput.read();
                        secondByte = bufSecondInput.read();
                        if (firstByte != secondByte) {
                            break;
                        }
                        if ((firstByte < 0) && (secondByte < 0)) {
                            retval = true;
                            break;
                        }
                    }
                } finally {
                    try {
                        if (bufFirstInput != null) {
                            bufFirstInput.close();
                        }
                    } finally {
                        if (bufSecondInput != null) {
                            bufSecondInput.close();
                        }
                    }
                }
            }
        }

        return retval;
    }


    public static HttpDownloadController prepareControllerTestFile() throws IOException {
        Files.deleteIfExists(Paths.get("test"));
        final String urlStr = "/test";
        stubFor(get(urlEqualTo(urlStr))
                .willReturn(aResponse()
                        .withBodyFile("test")));
        return new HttpDownloadController(new URL("http", "localhost", 8089, urlStr), null, 6000, 6000);
    }

}
