package ru.yandex.testtask.requests;

import org.junit.Assert;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;

/**
 * Created by Tronok on 22.06.14.
 */
public class HttpURIDownloadRequestTest {


    @Test(expected = NullPointerException.class)
    public void testConstructorPathsUrlsAreNull() throws URISyntaxException {
        new HttpURIDownloadRequest(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorUrlsAreNull() throws URISyntaxException {
        LinkedList<Path> paths = new LinkedList<>();
        paths.add(Paths.get("test_path"));
        new HttpURIDownloadRequest(null, paths);
    }

    @Test
    public void testConstructorPathsAreNull() throws MalformedURLException, URISyntaxException {
        LinkedList<URL> urls = new LinkedList<>();
        urls.add(new URL("http", "yandex.ru", 80, ""));
        new HttpURIDownloadRequest(urls, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorPathsSizeBiggerThanUrls() throws MalformedURLException, URISyntaxException {
        LinkedList<Path> paths = new LinkedList<>();
        paths.add(Paths.get("test_path"));
        paths.add(Paths.get("test_path2"));
        LinkedList<URL> urls = new LinkedList<>();
        urls.add(new URL("http", "yandex.ru", 80, ""));
        new HttpURIDownloadRequest(urls, paths);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorUrlsSizeBiggerThanPaths() throws MalformedURLException, URISyntaxException {
        LinkedList<Path> paths = new LinkedList<>();
        paths.add(Paths.get("test_path"));
        LinkedList<URL> urls = new LinkedList<>();
        urls.add(new URL("http", "yandex.ru", 80, ""));
        urls.add(new URL("http", "localhost", 80, ""));
        new HttpURIDownloadRequest(urls, paths);
    }

    @Test
    public void testConstructorEqualSizeOfCollection() throws MalformedURLException, URISyntaxException {
        LinkedList<Path> paths = new LinkedList<>();
        Path path1 = Paths.get("test_path");
        paths.add(path1);
        Path path2 = Paths.get("test_path2");
        paths.add(path2);
        LinkedList<URL> urls = new LinkedList<>();
        URL url1 = new URL("http", "yandex.ru", 80, "");
        urls.add(url1);
        URL url2 = new URL("http", "localhost", 80, "");
        urls.add(url2);
        HttpURIDownloadRequest request = new HttpURIDownloadRequest(urls, paths);
        Assert.assertArrayEquals(new URL[]{url1, url2}, request.getUrls().toArray());
        Assert.assertArrayEquals(new Path[]{path1, path2}, request.getPaths().toArray());
    }

}
