package ru.yandex.testtask.managers;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.google.code.tempusfugit.concurrency.annotations.Repeating;
import com.google.code.tempusfugit.temporal.Condition;
import com.google.code.tempusfugit.temporal.Duration;
import com.google.code.tempusfugit.temporal.Timeout;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import ru.yandex.testtask.exceptions.UnknownDownloadingException;
import ru.yandex.testtask.requests.DownloadRequestInterface;
import ru.yandex.testtask.requests.HttpURIDownloadRequest;
import ru.yandex.testtask.responses.DownloadResponse;
import ru.yandex.testtask.utilities.DownloadStatus;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.google.code.tempusfugit.temporal.WaitFor.waitOrTimeout;
import static ru.yandex.testtask.support.SupportTestUtils.isFileBinaryEqual;

/**
 * Created by Tronok on 22.06.14.
 */
public class UriDownloadManagerTest {
    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(8089);

    @Rule
    public WireMockClassRule instanceRule = wireMockRule;

    private final Path realFile = Paths.get("src/test/resources/__files/test");


    private void assertOnResults(Iterator<URL> urlIterator, Iterator<Path> pathIterator) throws IOException {
        while (urlIterator.hasNext() && pathIterator.hasNext()) {
            Path path = pathIterator.next();

            Assert.assertTrue("File " + path + " is bad", isFileBinaryEqual(path.toFile(), realFile.toFile()));
        }
    }

    private DownloadRequestInterface prepareRequestTwoTestFiles()
            throws IOException, TimeoutException, InterruptedException, URISyntaxException {
        return prepareRequestTwoTestFiles("test1", "test2");

    }


    private DownloadRequestInterface prepareRequestTwoTestFiles(final String pathName1, final String pathName2)
            throws IOException, TimeoutException, InterruptedException, URISyntaxException {
        Path path1 = Paths.get(pathName1);
        Path path2 = Paths.get(pathName2);

        final String urlStr1 = "/" + pathName1;
        final String urlStr2 = "/" + pathName2;
        LinkedList<URL> urls = new LinkedList<>();
        Collection<Path> paths = new LinkedList<>();
        stubFor(get(urlEqualTo(urlStr1))
                .willReturn(aResponse()
                        .withBodyFile("test")));
        final URL url1 = new URL("http", "localhost", 8089, urlStr1);
        urls.add(url1);
        try {
            UriDownloadManager.getInstance().stopAndDelete(url1);
        } catch (UnknownDownloadingException e) {
            e.printStackTrace();
        }
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                try {
                    UriDownloadManager.getInstance().getNotification(url1);
                } catch (UnknownDownloadingException e) {
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    return false;
                }
                return false;
            }
        }, Timeout.timeout(Duration.seconds(10)));
        paths.add(path1);
        stubFor(get(urlEqualTo(urlStr2))
                .willReturn(aResponse()
                        .withBodyFile("test")));
        paths.add(path2);
        final URL url2 = new URL("http", "localhost", 8089, urlStr2);
        urls.add(url2);
        try {
            UriDownloadManager.getInstance().stopAndDelete(url2);
        } catch (UnknownDownloadingException e) {
            e.printStackTrace();
        }
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                try {
                    UriDownloadManager.getInstance().getNotification(url2);
                } catch (UnknownDownloadingException e) {
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    return false;
                }
                return false;
            }
        }, Timeout.timeout(Duration.seconds(10)));
        Files.deleteIfExists(path1);
        Files.deleteIfExists(path2);
        return new HttpURIDownloadRequest(urls, paths);
    }

    private Runnable createTaskToDownload(final DownloadRequestInterface request, final CountDownLatch countDownLatch) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    countDownLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                UriDownloadManager.getInstance().download(request);
            }
        };
    }

    @Test
    @Repeating(repetition = 100)
    public void testDownloadCallFromWithinSingleThread() throws IOException, ExecutionException, InterruptedException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        for (Future<DownloadResponse> response : results.values()) {
            DownloadResponse dr = response.get();
            Assert.assertEquals(DownloadStatus.FINISHED, dr.getStatus());
        }
        Assert.assertTrue(isFileBinaryEqual(Paths.get("test1").toFile(),
                Paths.get("src/test/resources/__files/test").toFile()));
        Assert.assertTrue(isFileBinaryEqual(Paths.get("test2").toFile(),
                Paths.get("src/test/resources/__files/test").toFile()));
    }

    private void checkStatuses(Set<URI> results, DownloadStatus status) throws InterruptedException {
        boolean all = true;
        do {
            Thread.sleep(50);
            all = true;
            for (URI uri : results) {
                all = all && status ==
                        UriDownloadManager.getInstance().getNotification(uri).getStatus();
            }
        } while (!all);
    }

    @Test(timeout = 240000)
    @Repeating(repetition = 1000)
    public void testDownloadAndCancelByFutureFromWithinSingleThread() throws IOException, InterruptedException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        Thread.sleep(500);
        Iterator<Map.Entry<URI, Future<DownloadResponse>>> entryIterator = results.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<URI, Future<DownloadResponse>> next = entryIterator.next();
            if (!next.getValue().cancel(true)) {
                entryIterator.remove();
            }
        }

        Thread.sleep(1500);
        for (Future<DownloadResponse> response : results.values()) {
            try {
                response.get();
            } catch (CancellationException | ExecutionException e) {
            }
        }
        checkStatuses(results.keySet(), DownloadStatus.INTERRUPTED);

    }

    @Test(timeout = 240000)
    @Repeating(repetition = 1000)
    public void testDownloadCancelByMangerFromWithinSingleThread() throws IOException, InterruptedException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        Thread.sleep(500);
        Iterator<URI> uriIterator = results.keySet().iterator();
        while (uriIterator.hasNext()) {
            URI uri = uriIterator.next();
            if (!UriDownloadManager.getInstance().cancel(uri)) {
                uriIterator.remove();
            }
        }
        Thread.sleep(1500);
        for (Future<DownloadResponse> response : results.values()) {
            try {
                response.get();
            } catch (CancellationException | ExecutionException e) {
            }
        }
        checkStatuses(results.keySet(), DownloadStatus.INTERRUPTED);

    }

    private HashMap<URI, Future<DownloadResponse>> waitTillPaused()
            throws InterruptedException, TimeoutException, IOException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());

        for (URI uri : results.keySet()) {
            UriDownloadManager.getInstance().pause(uri);
        }
        Thread.sleep(1500);
        boolean allPaused = true;
        do {
            Thread.sleep(50);
            for (URI uri : results.keySet()) {
                allPaused = allPaused && DownloadStatus.PAUSED ==
                        UriDownloadManager.getInstance().getNotification(uri).getStatus();
            }
        } while (!allPaused);
        return results;
    }

    @Test(timeout = 240000)
    @Repeating(repetition = 100)
    public void testDownloadPauseFromWithinSingleThread() throws IOException, InterruptedException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results = waitTillPaused();
        for (URI uri : results.keySet()) {
            UriDownloadManager.getInstance().stopAndDelete(uri);
        }
    }

    private void waitTillRestartTaskEnd(Set<URI> results) throws ExecutionException, InterruptedException {
        LinkedList<Future<Future<DownloadResponse>>> newFutures = new LinkedList<>();
        for (URI uri : results) {
            newFutures.add(UriDownloadManager.getInstance().stopAndRestart(uri));
        }
        for (Future<Future<DownloadResponse>> future : newFutures) {
            Future<DownloadResponse> futureDeep = future.get();
            DownloadResponse dr = futureDeep.get();
            Assert.assertEquals(DownloadStatus.FINISHED, dr.getStatus());
        }
    }

    @Test(timeout = 240000)
    @Repeating(repetition = 1000)
    public void testStopAndRestartAfterPauseFromWithinSingleThread() throws ExecutionException, InterruptedException, IOException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results = waitTillPaused();
        waitTillRestartTaskEnd(results.keySet());
    }

    @Test
    @Repeating(repetition = 100)
    public void testDownloadPauseAndResumeFromWithinSingleThread() throws IOException, InterruptedException, ExecutionException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        for (URI uri : results.keySet()) {
            UriDownloadManager.getInstance().pause(uri);

        }
        Thread.sleep(1000);
        for (URI uri : results.keySet()) {
            UriDownloadManager.getInstance().resume(uri);
        }

        for (Future<DownloadResponse> response : results.values()) {
            DownloadResponse dr = response.get();
            Assert.assertEquals(DownloadStatus.FINISHED, dr.getStatus());
        }
    }

    @Test(expected = UnknownDownloadingException.class, timeout = 240000)
    @Repeating(repetition = 500)
    public void testStopAndDeleteFromWithinSingleThread() throws IOException, InterruptedException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        for (URI uri : results.keySet()) {
            UriDownloadManager.getInstance().stopAndDelete(uri);
        }
        Thread.sleep(1500);
        while (true) {
            for (URI uri : results.keySet()) {
                UriDownloadManager.getInstance().getNotification(uri);
            }
            Thread.sleep(50);
        }
    }

    @Test
    @Repeating(repetition = 100)
    public void testStopAndRestartAfterCompletionFromWithinSingleThread() throws ExecutionException, InterruptedException, IOException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        for (Future<DownloadResponse> response : results.values()) {
            DownloadResponse dr = response.get();
            Assert.assertEquals(DownloadStatus.FINISHED, dr.getStatus());
        }
        waitTillRestartTaskEnd(results.keySet());
    }


    @Test(timeout = 240000)
    @Repeating(repetition = 1000)
    public void testStopAndRestartAfterInterruptionFromWithinSingleThread() throws ExecutionException, InterruptedException, IOException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        Thread.sleep(500);
        Iterator<Map.Entry<URI, Future<DownloadResponse>>> entryIterator = results.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<URI, Future<DownloadResponse>> next = entryIterator.next();
            if (!next.getValue().cancel(true)) {
                entryIterator.remove();
            }
        }
        Thread.sleep(1500);
        for (Future<DownloadResponse> response : results.values()) {
            try {
                response.get();
            } catch (CancellationException | ExecutionException e) {
            }
        }
        checkStatuses(results.keySet(), DownloadStatus.INTERRUPTED);
        waitTillRestartTaskEnd(results.keySet());
    }

    @Test
    @Repeating(repetition = 1000)
    public void testStopAndRestartFromWithinSingleThread() throws ExecutionException, InterruptedException, IOException, TimeoutException, URISyntaxException {
        final HashMap<URI, Future<DownloadResponse>> results =
                UriDownloadManager.getInstance().download(prepareRequestTwoTestFiles());
        waitTillRestartTaskEnd(results.keySet());
    }

    @Test
    @Repeating(repetition = 100)
    public void testDownloadCallFromWithinTwoThread() throws IOException, TimeoutException, InterruptedException, URISyntaxException {

        final DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        final DownloadRequestInterface request2 = prepareRequestTwoTestFiles("test3", "test4");
        final LinkedList<URL> allURL = new LinkedList<>(request1.getUrls());
        allURL.addAll(request2.getUrls());
        LinkedList<Path> allPath = new LinkedList<>(request1.getPaths());
        allPath.addAll(request2.getPaths());
        final CountDownLatch latch = new CountDownLatch(1);
        final Thread thread1 = new Thread(createTaskToDownload(request1, latch));
        thread1.start();
        final Thread thread2 = new Thread(createTaskToDownload(request2, latch));
        thread2.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread1.getState() == Thread.State.TERMINATED && thread2.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(10)));
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {

                for (URL url : allURL) {
                    try {
                        if (UriDownloadManager.getInstance().getNotification(url).getStatus() != DownloadStatus.FINISHED) {
                            return false;
                        }
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                return true;
            }
        }, Timeout.timeout(Duration.seconds(30)));
        Iterator<URL> urlIterator = allURL.iterator();
        Iterator<Path> pathIterator = allPath.iterator();
        assertOnResults(urlIterator, pathIterator);

    }

    @Test
    @Repeating(repetition = 100)
    public void testDownloadAndPauseFromSeparateThreads() throws IOException, TimeoutException, InterruptedException, URISyntaxException {
        final DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        final CountDownLatch latch = new CountDownLatch(1);
        final Thread thread1 = new Thread(createTaskToDownload(request1, latch));
        final CountDownLatch latch2 = new CountDownLatch(1);
        thread1.start();
        Iterator<URL> urls = request1.getUrls().iterator();
        final URL toPause = urls.next();
        final Path toPausePath = request1.getPaths().iterator().next();
        final URL toFinish = urls.next();
        final Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch2.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    UriDownloadManager.getInstance().pause(toPause);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        });
        thread2.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread1.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(1)));
        latch2.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread2.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(1)));
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {

                try {
                    return UriDownloadManager.getInstance().getNotification(toPause).getStatus() == DownloadStatus.PAUSED &&
                            UriDownloadManager.getInstance().getNotification(toFinish).getStatus() == DownloadStatus.FINISHED;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }, Timeout.timeout(Duration.seconds(30)));
        try {
            isFileBinaryEqual(toPausePath.toFile(), realFile.toFile());
            Assert.fail();
        } catch (IOException ex) {

        }
    }

    private Thread createThreadWithActionAndLatch(final Action action, final CountDownLatch latch,
                                                  final URL toAnotherAction) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    action.makeSomethingOnManager(toAnotherAction);

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private URLsAndPath downloadAndAnotherAction(final Action action) throws TimeoutException, InterruptedException, IOException, URISyntaxException {
        final DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        final CountDownLatch latch = new CountDownLatch(1);
        final Thread thread1 = new Thread(createTaskToDownload(request1, latch));
        final CountDownLatch latch2 = new CountDownLatch(1);
        thread1.start();
        Iterator<URL> urls = request1.getUrls().iterator();
        final URL toAnotherAction = urls.next();
        final Path toAnotherActionPath = request1.getPaths().iterator().next();
        final URL toFinish = urls.next();
        final Thread thread2 = createThreadWithActionAndLatch(action, latch2, toAnotherAction);
        thread2.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread1.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(10)));
        latch2.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread2.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(10)));
        return new URLsAndPath(toFinish, toAnotherAction, toAnotherActionPath);
    }

    private Action createPauseAction() {
        return new Action() {
            @Override
            public void makeSomethingOnManager(URL url) throws URISyntaxException {
                UriDownloadManager.getInstance().pause(url);
            }
        };
    }

    private Action createCancelAction() {
        return new Action() {
            @Override
            public void makeSomethingOnManager(URL url) throws URISyntaxException {
                UriDownloadManager.getInstance().cancel(url);
            }
        };
    }

    private Action createStopAndDeleteAction() {
        return new Action() {
            @Override
            public void makeSomethingOnManager(URL url) throws URISyntaxException {
                UriDownloadManager.getInstance().stopAndDelete(url);
            }
        };
    }

    private Action createStopAndRestartAction() {
        return new Action() {
            @Override
            public void makeSomethingOnManager(URL url) throws URISyntaxException {
                UriDownloadManager.getInstance().stopAndRestart(url);
            }
        };
    }

    private void testFileAndStatuses(final URLsAndPath urlsAndPath, final DownloadStatus status, boolean assertFileTrue)
            throws TimeoutException, InterruptedException, IOException {
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                try {
                    return UriDownloadManager.getInstance().getNotification(urlsAndPath.toAnotherAction).getStatus() == status &&
                            UriDownloadManager.getInstance().getNotification(urlsAndPath.toFinish).getStatus() == DownloadStatus.FINISHED;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    return false;
                } catch (UnknownDownloadingException e) {
                    return false;
                }
            }
        }, Timeout.timeout(Duration.seconds(30)));
        if (assertFileTrue) {
            Assert.assertTrue("File " + urlsAndPath.toAnotherActionPath + " is bad",
                    isFileBinaryEqual(urlsAndPath.toAnotherActionPath.toFile(), realFile.toFile()));

        } else {
            Assert.assertFalse("File " + urlsAndPath.toAnotherActionPath + " is bad",
                    isFileBinaryEqual(urlsAndPath.toAnotherActionPath.toFile(), realFile.toFile()));
        }
    }

    private URLsAndPath downloadAndTwoAnotherActionOnTheSameDownload(final Action actionFirst, final Action actionSecond)
            throws TimeoutException, InterruptedException, IOException, URISyntaxException {
        final DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        final CountDownLatch latch = new CountDownLatch(1);
        final Thread thread1 = new Thread(createTaskToDownload(request1, latch));
        final CountDownLatch latch2 = new CountDownLatch(1);
        thread1.start();
        Iterator<URL> urls = request1.getUrls().iterator();
        final URL toAnotherAction = urls.next();
        final Path toAnotherActionPath = request1.getPaths().iterator().next();
        final URL toFinish = urls.next();
        final Thread thread2 = createThreadWithActionAndLatch(actionFirst, latch2, toAnotherAction);
        thread2.start();
        final Thread thread3 = createThreadWithActionAndLatch(actionSecond, latch2, toAnotherAction);
        thread3.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread1.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(1)));
        latch2.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread2.getState() == Thread.State.TERMINATED && thread3.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(1)));
        return new URLsAndPath(toFinish, toAnotherAction, toAnotherActionPath);
    }

    @Test
    @Repeating(repetition = 100)
    public void testDownloadAndPauseAndResumeFromSeparateThreads() throws IOException, TimeoutException, InterruptedException, URISyntaxException {
        final URLsAndPath urlsAndPath = downloadAndAnotherAction(createPauseAction());
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                try {
                    return UriDownloadManager.getInstance().getNotification(urlsAndPath.toAnotherAction).getStatus() == DownloadStatus.PAUSED &&
                            UriDownloadManager.getInstance().getNotification(urlsAndPath.toFinish).getStatus() == DownloadStatus.FINISHED;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }, Timeout.timeout(Duration.seconds(30)));
        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    UriDownloadManager.getInstance().resume(urlsAndPath.toAnotherAction);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        });
        thread3.start();
        testFileAndStatuses(urlsAndPath, DownloadStatus.FINISHED, true);
    }

    @Test
    @Repeating(repetition = 100)
    public void testDownloadAndInterruptFromSeparateThreads() throws IOException, TimeoutException, InterruptedException, URISyntaxException {
        final URLsAndPath urlsAndPath = downloadAndAnotherAction(createCancelAction());
        testFileAndStatuses(urlsAndPath, DownloadStatus.INTERRUPTED, false);
    }

    @Test(timeout = 240000)
    @Repeating(repetition = 100)
    public void testDownloadAndDelete() throws IOException, TimeoutException, InterruptedException, ExecutionException, URISyntaxException {
        final URLsAndPath urlsAndPath = downloadAndAnotherAction(createStopAndDeleteAction());
        Future<DownloadResponse> future = UriDownloadManager.getInstance().getFuture(urlsAndPath.toFinish);
        DownloadResponse response = future.get();
        Assert.assertEquals(DownloadStatus.FINISHED, response.getStatus());
        try {
            while (true) {
                UriDownloadManager.getInstance().getNotification(urlsAndPath.toAnotherAction);
                Thread.sleep(50);
            }
        } catch (UnknownDownloadingException e) {
            Assert.assertFalse(isFileBinaryEqual(urlsAndPath.toAnotherActionPath.toFile(), realFile.toFile()));
        }


    }

    @Test
    @Repeating(repetition = 100)
    public void testDownloadAndPauseAndInterruptPausedFromSeparateThreads() throws InterruptedException, TimeoutException, IOException, URISyntaxException {
        final URLsAndPath urlsAndPath = downloadAndTwoAnotherActionOnTheSameDownload(createPauseAction(),
                createCancelAction());
        try {
            testFileAndStatuses(urlsAndPath, DownloadStatus.INTERRUPTED, false);
        } catch (TimeoutException e) {
            testFileAndStatuses(urlsAndPath, DownloadStatus.PAUSED, false);
        }
    }

    @Test
    @Repeating(repetition = 1000)
    public void testGetFuture() throws InterruptedException, TimeoutException, IOException, URISyntaxException, ExecutionException {
        final DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        UriDownloadManager.getInstance().download(request1);
        Iterator<URL> urls = request1.getUrls().iterator();
        final URL forFuture = urls.next();
        Future<DownloadResponse> request = UriDownloadManager.getInstance().getFuture(forFuture);
        DownloadResponse request2 = request.get();
        Assert.assertEquals(DownloadStatus.FINISHED, request2.getStatus());
    }

    @Test
    @Repeating(repetition = 1000)
    public void testRestartAndInterruptFromSeparateThreads() throws InterruptedException, TimeoutException, IOException, URISyntaxException {
        final URLsAndPath urlsAndPath = downloadAndTwoAnotherActionOnTheSameDownload(createStopAndRestartAction(),
                createCancelAction());
        try {
            testFileAndStatuses(urlsAndPath, DownloadStatus.FINISHED, true);
        } catch (TimeoutException e) {
            testFileAndStatuses(urlsAndPath, DownloadStatus.INTERRUPTED, false);
        }
    }

    @Test
    @Repeating(repetition = 1000)
    public void testTheSameTaskStartFromSeparatedThreads() throws InterruptedException, TimeoutException, IOException, URISyntaxException {
        final DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        final CountDownLatch latch = new CountDownLatch(1);
        final Thread thread1 = new Thread(createTaskToDownload(request1, latch));
        final Thread thread2 = new Thread(createTaskToDownload(request1, latch));
        thread2.start();
        thread1.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread1.getState() == Thread.State.TERMINATED && thread2.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(1)));
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                for (URL url : request1.getUrls()) {
                    try {
                        if (UriDownloadManager.getInstance().getNotification(url).getStatus() != DownloadStatus.FINISHED) {
                            return false;
                        }
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                return true;
            }
        }, Timeout.timeout(Duration.seconds(30)));
        Iterator<URL> urls = request1.getUrls().iterator();
        Iterator<Path> paths = request1.getPaths().iterator();
        assertOnResults(urls, paths);
    }

    @Test
    @Repeating(repetition = 100)
    public void testNullFutures() throws InterruptedException, TimeoutException, IOException, URISyntaxException {
        final DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        HashMap<URI, Future<DownloadResponse>> futures = UriDownloadManager.getInstance().download(request1);
        for (Future<DownloadResponse> future : futures.values()) {
            Assert.assertNotNull(future);
        }
        futures = UriDownloadManager.getInstance().download(request1);
        for (Future<DownloadResponse> future : futures.values()) {
            Assert.assertNull(future);
        }
    }

    @Test
    @Repeating(repetition = 10000)
    public void testDoubleStopAndStartFromSeparateThreads() throws InterruptedException, URISyntaxException, TimeoutException, IOException {
        final URLsAndPath urlsAndPath = downloadAndTwoAnotherActionOnTheSameDownload(createStopAndRestartAction(),
                createStopAndRestartAction());
        testFileAndStatuses(urlsAndPath, DownloadStatus.FINISHED, true);
    }

    private interface Action {
        void makeSomethingOnManager(URL url) throws URISyntaxException;
    }

    private class URLsAndPath {
        final URL toFinish;
        final URL toAnotherAction;
        final Path toAnotherActionPath;

        private URLsAndPath(URL toFinish, URL toAnotherAction, Path toAnotherActionPath) {
            this.toFinish = toFinish;
            this.toAnotherAction = toAnotherAction;
            this.toAnotherActionPath = toAnotherActionPath;
        }
    }

    @Test
    @Repeating(repetition = 1000)
    public void testClearFinishedDownloads() throws InterruptedException, URISyntaxException, TimeoutException, IOException {
        DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        final CountDownLatch latch = new CountDownLatch(1);
        Iterator<URL> urlIterator = request1.getUrls().iterator();
        URL url1 = urlIterator.next();
        URL url2 = urlIterator.next();
        Path path1 = request1.getPaths().iterator().next();
        final Thread thread1 = new Thread(createTaskToDownload(request1, latch));
        thread1.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread1.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(1)));
        testFileAndStatuses(new URLsAndPath(url2, url1, path1), DownloadStatus.FINISHED, true);
        DownloadRequestInterface request2 = prepareRequestTwoTestFiles("test3", "test4");
        final CountDownLatch latch2 = new CountDownLatch(1);
        final Thread thread2 = new Thread(createTaskToDownload(request2, latch2));
        thread2.start();
        latch2.countDown();
        List<URL> urls = UriDownloadManager.getInstance().clearFinishedDownloads();
        Assert.assertTrue(urls.contains(url1) && urls.contains(url2));
    }


    private void waitTillDeleted(boolean shutdown) throws InterruptedException, URISyntaxException, TimeoutException, IOException {
        DownloadRequestInterface request1 = prepareRequestTwoTestFiles("test1", "test2");
        Iterator<URL> urlIterator = request1.getUrls().iterator();
        final URL url1 = urlIterator.next();
        final URL url2 = urlIterator.next();
        final CountDownLatch latch = new CountDownLatch(1);
        final Thread thread1 = new Thread(createTaskToDownload(request1, latch));
        thread1.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return thread1.getState() == Thread.State.TERMINATED;
            }
        }, Timeout.timeout(Duration.seconds(1)));
        if (shutdown) {
            UriDownloadManager.getInstance().shutdownAllDownloads();
        }
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                boolean caught1 = false, caught2 = false;
                try {
                    UriDownloadManager.getInstance().getNotification(url1);
                } catch (UnknownDownloadingException e) {
                    caught1 = true;
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                try {
                    UriDownloadManager.getInstance().getNotification(url2);
                } catch (UnknownDownloadingException e) {
                    caught2 = true;
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                return caught1 && caught2;
            }
        }, Timeout.timeout(Duration.seconds(30)));
    }

    @Test
    @Repeating(repetition = 1000)
    public void testAutoClearing() throws InterruptedException, URISyntaxException, TimeoutException, IOException {
        UriDownloadManager.getInstance().enableAutoClearingFinished(0, 100, TimeUnit.MILLISECONDS);
        waitTillDeleted(false);
        waitTillDeleted(false);
        UriDownloadManager.getInstance().disableAutoClearingFinished();
    }

    @Test(expected = TimeoutException.class)
    @Repeating(repetition = 1000)
    public void testAutoClearingDisable() throws InterruptedException, IOException, TimeoutException, URISyntaxException {
        try {
            UriDownloadManager.getInstance().enableAutoClearingFinished(0, 100, TimeUnit.MILLISECONDS);
            waitTillDeleted(false);
        } catch (TimeoutException e) {
            Assert.fail();
        }
        UriDownloadManager.getInstance().disableAutoClearingFinished();
        Thread.sleep(200);
        waitTillDeleted(false);
    }

    @Test
    @Repeating(repetition = 1000)
    public void testShutdown() throws InterruptedException, IOException, TimeoutException, URISyntaxException {
        waitTillDeleted(true);
    }

}
