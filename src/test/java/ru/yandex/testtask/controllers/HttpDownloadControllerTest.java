package ru.yandex.testtask.controllers;

import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.google.code.tempusfugit.concurrency.annotations.Repeating;
import com.google.code.tempusfugit.temporal.Condition;
import com.google.code.tempusfugit.temporal.Duration;
import com.google.code.tempusfugit.temporal.Timeout;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import ru.yandex.testtask.responses.DownloadResponse;
import ru.yandex.testtask.utilities.DownloadStatus;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.channels.FileLock;
import java.nio.file.Paths;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.google.code.tempusfugit.temporal.Conditions.isWaiting;
import static com.google.code.tempusfugit.temporal.WaitFor.waitOrTimeout;
import static org.hamcrest.core.Is.is;
import static ru.yandex.testtask.support.SupportTestUtils.isFileBinaryEqual;
import static ru.yandex.testtask.support.SupportTestUtils.prepareControllerTestFile;

/**
 * Created by Tronok on 21.06.14.
 */

public class HttpDownloadControllerTest {
    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(8089);

    @Rule
    public WireMockClassRule instanceRule = wireMockRule;

    private Condition createInterruptCondition(final HttpDownloadController controller) {
        return new Condition() {
            @Override
            public boolean isSatisfied() {
                return controller.getNotificationToken().getStatus() == DownloadStatus.INTERRUPTED;
            }
        };
    }

    private Condition createRunCondition(final HttpDownloadController controller) {
        return new Condition() {
            @Override
            public boolean isSatisfied() {
                return controller.getNotificationToken().getStatus() == DownloadStatus.RUN;
            }
        };
    }

    private Condition createFinishCondition(final HttpDownloadController controller) {
        return new Condition() {
            @Override
            public boolean isSatisfied() {
                return controller.getNotificationToken().getStatus() == DownloadStatus.FINISHED;
            }
        };
    }

    private Condition createFailCondition(final HttpDownloadController controller) {
        return new Condition() {
            @Override
            public boolean isSatisfied() {
                return controller.getNotificationToken().getStatus() == DownloadStatus.FAILED;
            }
        };
    }

    private Runnable runController(final HttpDownloadController controller) {
        return new Runnable() {
            @Override
            public void run() {
                controller.call();
            }
        };
    }


    private ControllerAndThread pauseController() throws TimeoutException, InterruptedException, IOException {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        thread.start();
        waitOrTimeout(createRunCondition(controller), Timeout.timeout(Duration.millis(500)));
        controller.pause();
        waitOrTimeout(isWaiting(thread), Timeout.timeout(Duration.seconds(10)));
        return new ControllerAndThread(controller, thread);
    }

    @Test
    @Repeating(repetition = 10)
    public void testInterrupt() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        thread.start();
        waitOrTimeout(createRunCondition(controller), Timeout.timeout(Duration.millis(500)));
        thread.interrupt();
        waitOrTimeout(createInterruptCondition(controller), Timeout.timeout(Duration.seconds(1)));
    }

    @Test
    @Repeating(repetition = 100)
    public void testInterruptByMethod() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        thread.start();
        waitOrTimeout(createRunCondition(controller), Timeout.timeout(Duration.millis(500)));
        controller.cancel();
        waitOrTimeout(createInterruptCondition(controller), Timeout.timeout(Duration.seconds(1)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testPauseAndResume() throws Exception {
        final ControllerAndThread cat = pauseController();
        cat.controller.resume();
        waitOrTimeout(createFinishCondition(cat.controller), Timeout.timeout(Duration.seconds(10)));
        Assert.assertTrue(isFileBinaryEqual(Paths.get("test").toFile(),
                Paths.get("src/test/resources/__files/test").toFile()));
    }

    @Test
    @Repeating(repetition = 10)
    public void testPauseAndInterrupt() throws Exception {
        final ControllerAndThread cat = pauseController();
        cat.thread.interrupt();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
        Assert.assertFalse(isFileBinaryEqual(Paths.get("test").toFile(),
                Paths.get("src/test/resources/__files/test").toFile()));
    }

    @Test
    @Repeating(repetition = 10)
    public void testPauseInterruptResume() throws Exception {
        final ControllerAndThread cat = pauseController();
        cat.thread.interrupt();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
        cat.controller.resume();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testPauseInterruptPause() throws Exception {
        final ControllerAndThread cat = pauseController();
        cat.thread.interrupt();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
        cat.controller.pause();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testPauseInterruptPauseResume() throws Exception {
        final ControllerAndThread cat = pauseController();
        cat.thread.interrupt();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
        cat.controller.pause();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
        cat.controller.resume();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testPauseInterruptResumePause() throws Exception {
        final ControllerAndThread cat = pauseController();
        cat.thread.interrupt();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
        cat.controller.resume();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
        cat.controller.pause();
        waitOrTimeout(createInterruptCondition(cat.controller), Timeout.timeout(Duration.seconds(1)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testFileLocked() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                controller.call();
            }
        });

        try (RandomAccessFile randomAccessFile = new RandomAccessFile("test", "rw");
             FileLock lock = randomAccessFile.getChannel().lock()) {
            Assert.assertTrue(lock.isValid());
            thread.start();
            waitOrTimeout(createFailCondition(controller), Timeout.timeout(Duration.seconds(10)));
        }

    }

    @Test
    @Repeating(repetition = 10)
    public void testSimultaneouslyDownloadTwoFiles() throws Exception {
        final String urlStr = "/test";
        stubFor(get(urlEqualTo(urlStr))
                .willReturn(aResponse()
                        .withBodyFile("test")));

        final HttpDownloadController controller1 = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr),
                Paths.get("test1"), 6000, 6000);
        final HttpDownloadController controller2 = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr),
                Paths.get("test2"), 6000, 6000);
        final CountDownLatch latch = new CountDownLatch(1);
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                controller1.call();

            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                controller2.call();

            }
        });
        thread1.start();
        thread2.start();
        latch.countDown();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return controller1.getNotificationToken().getStatus() == DownloadStatus.FINISHED &&
                        controller2.getNotificationToken().getStatus() == DownloadStatus.FINISHED;
            }
        }, Timeout.timeout(Duration.seconds(10)));
    }

    @Test
    @Repeating(repetition = 1000)
    public void testPauseInterruptAndStartAgainOfTwoTasks() throws IOException, TimeoutException, InterruptedException {
        final String urlStr1 = "/test1";
        stubFor(get(urlEqualTo(urlStr1))
                .willReturn(aResponse()
                        .withBodyFile("test")));
        final String urlStr2 = "/test2";
        stubFor(get(urlEqualTo(urlStr2))
                .willReturn(aResponse()
                        .withBodyFile("test")));
        final HttpDownloadController controller1 =
                new HttpDownloadController(new URL("http", "localhost", 8089, urlStr1), null, 6000, 6000);
        final HttpDownloadController controller3 =
                new HttpDownloadController(new URL("http", "localhost", 8089, urlStr2), null, 6000, 6000);
        Thread thread1 = new Thread(runController(controller1));
        Thread thread3 = new Thread(runController(controller3));
        thread1.start();
        thread3.start();
        waitOrTimeout(createRunCondition(controller1), Timeout.timeout(Duration.seconds(1)));
        waitOrTimeout(createRunCondition(controller3), Timeout.timeout(Duration.seconds(1)));
        controller1.pause();
        controller3.pause();
        waitOrTimeout(isWaiting(thread1),
                Timeout.timeout(Duration.seconds(10)));
        waitOrTimeout(isWaiting(thread3),
                Timeout.timeout(Duration.seconds(10)));
        thread1.interrupt();
        thread3.interrupt();
        waitOrTimeout(createInterruptCondition(controller1),
                Timeout.timeout(Duration.seconds(10)));
        waitOrTimeout(createInterruptCondition(controller3),
                Timeout.timeout(Duration.seconds(10)));
        final HttpDownloadController controller2 =
                new HttpDownloadController(new URL("http", "localhost", 8089, urlStr1), null, 6000, 6000);
        final HttpDownloadController controller4 =
                new HttpDownloadController(new URL("http", "localhost", 8089, urlStr2), null, 6000, 6000);
        Thread thread2 = new Thread(runController(controller2));
        Thread thread4 = new Thread(runController(controller4));
        thread2.start();
        thread4.start();
        waitOrTimeout(createFinishCondition(controller2), Timeout.timeout(Duration.seconds(10)));
        waitOrTimeout(createFinishCondition(controller4), Timeout.timeout(Duration.seconds(10)));
    }

    @Test
    @Repeating(repetition = 100)
    public void testSimultaneouslyDownloadToTheSameFile() throws Exception {
        final String urlStr = "/test";
        stubFor(get(urlEqualTo(urlStr))
                .willReturn(aResponse()
                        .withBodyFile("test")));

        final HttpDownloadController controller1 = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr),
                Paths.get("test1"), 6000, 6000);
        final HttpDownloadController controller2 = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr),
                Paths.get("test1"), 6000, 6000);

        Thread thread1 = new Thread(runController(controller1));
        Thread thread2 = new Thread(runController(controller2));
        thread1.start();
        waitOrTimeout(createRunCondition(controller1), Timeout.timeout(Duration.seconds(1)));
        thread2.start();
        waitOrTimeout(new Condition() {
            @Override
            public boolean isSatisfied() {
                return controller1.getNotificationToken().getStatus() == DownloadStatus.FINISHED &&
                        controller2.getNotificationToken().getStatus() == DownloadStatus.FAILED ||
                        controller2.getNotificationToken().getStatus() == DownloadStatus.FINISHED &&
                                controller1.getNotificationToken().getStatus() == DownloadStatus.FAILED;
            }
        }, Timeout.timeout(Duration.seconds(20)));
    }

    @Test
    public void testGetStatusRunFinished() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        thread.start();
        waitOrTimeout(createRunCondition(controller), Timeout.timeout(Duration.seconds(10)));
        waitOrTimeout(createFinishCondition(controller), Timeout.timeout(Duration.seconds(10)));
    }

    @Test
    public void testCallDownloadWithFullName() throws Exception {
        HttpDownloadController controller = prepareControllerTestFile();
        DownloadResponse response = controller.call();
        DownloadResponse expected = new DownloadResponse(DownloadStatus.FINISHED, null, Paths.get("test"));
        Assert.assertEquals(expected, response);
        Assert.assertTrue(isFileBinaryEqual(expected.getPathToFile().toFile(),
                Paths.get("src/test/resources/__files/test").toFile()));
    }

    @Test
    public void testCallDownloadWithHeader() throws Exception {
        final String urlStr = "/file-for-test";
        stubFor(get(urlEqualTo(urlStr))
                .willReturn(aResponse()
                        .withBodyFile("test").withHeader("Content-Disposition", "attachment; filename=\"test\"")));
        HttpDownloadController controller = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr),
                null, 6000, 6000);
        DownloadResponse response = controller.call();
        DownloadResponse expected = new DownloadResponse(DownloadStatus.FINISHED, null, Paths.get("test"));
        Assert.assertEquals(expected, response);

    }

    @Test
    public void testCallDownloadServerBadRequest() throws Exception {
        final String urlStr = "/file-for-test";
        stubFor(get(urlEqualTo(urlStr))
                .willReturn(aResponse()
                        .withStatus(400)));
        HttpDownloadController controller = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr),
                Paths.get("test"), 6000, 6000);
        DownloadResponse response = controller.call();
        Assert.assertThat(response.getStatus(), is(DownloadStatus.FAILED));
    }

    @Test
    public void testCallDownloadServerOkResponseAndClose() throws Exception {
        final String urlStr = "/test";
        stubFor(get(urlEqualTo(urlStr))
                .willReturn(aResponse().withStatus(200).withFixedDelay(2000)
                        .withBodyFile("test").withFault(Fault.MALFORMED_RESPONSE_CHUNK)));
        final HttpDownloadController controller = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr),
                null, 6000, 6000);
        DownloadResponse response = controller.call();
        Assert.assertEquals(DownloadStatus.FAILED, response.getStatus());
        Assert.assertFalse(isFileBinaryEqual(response.getPathToFile().toFile(),
                Paths.get("src/test/resources/__files/test").toFile()));
        final String urlStr2 = "/test2";
        stubFor(get(urlEqualTo(urlStr))
                .willReturn(aResponse().withStatus(200).withFixedDelay(2000)
                        .withBodyFile("test").withFault(Fault.RANDOM_DATA_THEN_CLOSE)));
        final HttpDownloadController controller2 = new HttpDownloadController(new URL("http", "localhost", 8089, urlStr2),
                null, 6000, 6000);
        DownloadResponse response2 = controller2.call();
        Assert.assertEquals(DownloadStatus.FAILED, response2.getStatus());
        Assert.assertNull(response2.getPathToFile());
    }

    @Test
    public void testPauseBeforeRun() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        controller.pause();
        thread.start();
        waitOrTimeout(isWaiting(thread),
                Timeout.timeout(Duration.seconds(10)));
        controller.resume();
        waitOrTimeout(createFinishCondition(controller), Timeout.timeout(Duration.seconds(10)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testDoubleInterrupt() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        thread.start();
        waitOrTimeout(createRunCondition(controller), Timeout.timeout(Duration.seconds(1)));
        thread.interrupt();
        Thread.sleep(500);
        thread.interrupt();
        waitOrTimeout(createInterruptCondition(controller), Timeout.timeout(Duration.seconds(10)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testResumeWithoutPause() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        thread.start();
        waitOrTimeout(createRunCondition(controller), Timeout.timeout(Duration.seconds(1)));
        controller.resume();
        waitOrTimeout(createFinishCondition(controller), Timeout.timeout(Duration.seconds(10)));
    }

    @Test
    @Repeating(repetition = 10)
    public void testDoublePause() throws Exception {
        final HttpDownloadController controller = prepareControllerTestFile();
        Thread thread = new Thread(runController(controller));
        thread.start();
        waitOrTimeout(createRunCondition(controller), Timeout.timeout(Duration.seconds(1)));
        controller.pause();
        Thread.sleep(500);
        controller.pause();
        waitOrTimeout(isWaiting(thread), Timeout.timeout(Duration.seconds(10)));
    }

    private class ControllerAndThread {
        final HttpDownloadController controller;
        final Thread thread;

        ControllerAndThread(HttpDownloadController controller, Thread thread) {
            this.controller = controller;
            this.thread = thread;
        }
    }
}
