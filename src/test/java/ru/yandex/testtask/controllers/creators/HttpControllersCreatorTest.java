package ru.yandex.testtask.controllers.creators;

import org.junit.Assert;
import org.junit.Test;
import ru.yandex.testtask.controllers.DownloadControllerInterface;
import ru.yandex.testtask.requests.HttpURIDownloadRequest;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by Tronok on 22.06.14.
 */
public class HttpControllersCreatorTest {

    private void createControllerTestWithPaths(Path path1, Path path2, boolean pathNullList) throws MalformedURLException, URISyntaxException {
        LinkedList<Path> paths = new LinkedList<>();
        paths.add(path1);
        paths.add(path2);
        LinkedList<URL> urls = new LinkedList<>();
        URL url1 = new URL("http", "yandex.ru", 80, "");
        urls.add(url1);
        URL url2 = new URL("http", "localhost", 80, "");
        urls.add(url2);
        if (pathNullList) {
            paths = null;
        }
        HttpURIDownloadRequest request = new HttpURIDownloadRequest(urls, paths);
        HttpControllersCreator creator = new HttpControllersCreator(request);
        Collection<DownloadControllerInterface> controllers = creator.createControllers();
        Assert.assertEquals(2, controllers.size());
        Iterator<DownloadControllerInterface> iterator = controllers.iterator();
        DownloadControllerInterface controller = iterator.next();
        Assert.assertEquals(url1, controller.getUrl());
        Assert.assertEquals(path1, controller.getPathToSave());
        controller = iterator.next();
        Assert.assertEquals(url2, controller.getUrl());
        Assert.assertEquals(path2, controller.getPathToSave());
    }

    @Test
    public void testCreateController() throws MalformedURLException, URISyntaxException {
        createControllerTestWithPaths(Paths.get("test_path1"), Paths.get("test_path2"), false);
    }

    @Test
    public void testCreateControllerWithNullPath() throws Exception {
        createControllerTestWithPaths(Paths.get("test_path1"), null, false);
    }

    @Test
    public void testCreateControllerWithoutPaths() throws MalformedURLException, URISyntaxException {
        createControllerTestWithPaths(null, null, true);
    }
}
