package ru.yandex.testtask.utilities;

/**
 * Different states of downloading.
 */
public enum DownloadStatus {
    NOT_STARTED,
    RUN,
    RUN_AFTER_PAUSED,
    PAUSED,
    INTERRUPTED, //canceled or thread was interrupted.
    FAILED,
    FINISHED,
}
