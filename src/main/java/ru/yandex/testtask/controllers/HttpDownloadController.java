package ru.yandex.testtask.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.testtask.exceptions.UnableToLockFile;
import ru.yandex.testtask.exceptions.UnacceptableServerResponse;
import ru.yandex.testtask.responses.DownloadResponse;
import ru.yandex.testtask.utilities.DownloadStatus;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Provides functionality to download one file over HTTP without authorization.
 * Pause, resume and cancel methods are fair realization. Class is thread-safe, but with blocking.
 * <p>
 * Note that thread downloading can be interrupted and in that case you have to change status in both StatusNotification
 * and  DownloadResponse object to DownloadStatus.INTERRUPTED. NotificationStatus object have to be the same object among all returns
 * and is held inside controller between any method calls.
 * </p>
 *
 * @see ru.yandex.testtask.responses.DownloadResponse
 */
public class HttpDownloadController extends DownloadController {


    private final static Logger LOGGER = LoggerFactory.getLogger(HttpDownloadController.class);
    private final Object locker;
    private final StatusNotificationRealization notification;
    private final int connectionTimeout;
    private final int readTimeout;
    private Exception reason;
    private final AtomicLong downloaded;

    /**
     * Creates HttpDownloadController from urls and paths
     *
     * @param url        URL of downloading.
     * @param pathToSave path where data will be saved.
     * @throws java.lang.NullPointerException thrown if URl is null.
     */
    public HttpDownloadController(URL url, Path pathToSave, int connectionTimeout, int readTimeout) {
        LOGGER.debug("Creation of download controller with url " + url + " was started.");
        if (url == null) {
            throw new NullPointerException("URL must be not null");
        }
        setUrl(url);
        setPathToSave(pathToSave);
        status = DownloadStatus.NOT_STARTED;
        locker = new Object();
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
        downloaded = new AtomicLong(0);
        notification = new StatusNotificationRealization(status, url);
        LOGGER.debug("Creation of download controller with url " + url + " was finished.");
    }

    /**
     * Tells download process to pause ASAP (when current block of data will be downloaded) if it was not started or it's running.
     *
     * @return notification with status DownloadStatus.PAUSED
     * @see ru.yandex.testtask.utilities.DownloadStatus
     */
    @Override
    public StatusNotification pause() {
        synchronized (locker) {
            if (status == DownloadStatus.RUN || status == DownloadStatus.NOT_STARTED) {
                status = DownloadStatus.PAUSED;
                notification.setStatus(status);
            }
            LOGGER.debug("download controller with url " + getUrl() + " was paused");
            return notification;
        }
    }

    /**
     * Tells download process to resume ASAP if current state is paused. Otherwise nothing will happen.
     *
     * @return notification with status DownloadStatus.RUN
     */
    @Override
    public StatusNotification resume() {
        synchronized (locker) {
            if (status == DownloadStatus.PAUSED) {
                status = DownloadStatus.RUN_AFTER_PAUSED;
                //notification.setStatus(status);
                locker.notify();
                LOGGER.debug("download controller with url " + getUrl() + " was resumed");

            }
            return notification;
        }

    }

    /**
     * Tells download process to interrupt ASAP. Interruption will not happen unless current status os finished or failed.
     *
     * @return notification with current status. It's not necessary interrupted.
     */
    @Override
    public StatusNotification cancel() {
        synchronized (locker) {
            if (status != DownloadStatus.FINISHED && status != DownloadStatus.FAILED) {
                status = DownloadStatus.INTERRUPTED;
                LOGGER.debug("download controller with url " + getUrl() + " was interrupted by method call");
                locker.notify();
            }
            return notification;
        }

    }

    /**
     * Gets StatusNotification object that's specific for controller
     *
     * @return notification object.
     */
    @Override
    public StatusNotification getNotificationToken() {
        synchronized (locker) {
            return notification;
        }
    }

    /**
     * Tests object on equality. Note that if o is HttpDownloadController when equality means:
     * that paths are equal; if URIs can be obtained from URLs when tests URIs equality otherwise tests URLs equality.
     * Reason of such comparison strategy is bad URL equal method implementation.
     *
     * @param o another object for comparison.
     * @return true if object are equal otherwise false.
     * @see java.net.URL
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HttpDownloadController)) return false;

        HttpDownloadController that = (HttpDownloadController) o;

        URL url = getUrl();
        URL thatUrl = that.getUrl();
        if (url != null && thatUrl != null) {
            try {
                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                        url.getPath(), url.getQuery(), url.getRef());
                URI thatUri = new URI(thatUrl.getProtocol(), thatUrl.getUserInfo(), thatUrl.getHost(), thatUrl.getPort(),
                        thatUrl.getPath(), thatUrl.getQuery(), thatUrl.getRef());
                if (!uri.equals(thatUri)) {
                    return false;
                }
            } catch (URISyntaxException e) {
                if (!url.equals(thatUrl)) {
                    return false;
                }
            }
        } else if (!(url == null && thatUrl == null)) {
            return false;
        }
        return !(getPathToSave() != null ? !getPathToSave().equals(that.getPathToSave()) : that.getPathToSave() != null);
    }

    /**
     * Computes hash code base on path hash and URI hash code if URL can be represented as URI. Otherwise URL hash code
     * is used instead of URI.
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        int result = (getPathToSave() != null ? getPathToSave().hashCode() : 0);
        URL url = getUrl();
        try {
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                    url.getPath(), url.getQuery(), url.getRef());
            result += 31 * result + uri.hashCode();
        } catch (URISyntaxException e) {
            result += 31 * result + getUrl().hashCode();
        }
        return result;
    }

    /**
     * Download file over HTTP into specified of obtained path. If path was set null,
     * name of file will be obtained from HTTP header or URL  and file will saved in current directory.
     *
     * @return response with result state after downloading.
     */
    @Override
    public DownloadResponse call() {
        updateStatusToRun();
        HttpURLConnection connection = null;
        try {
            connection = initializeConnection();
            LOGGER.debug("Connection established in download controller with url " + getUrl());
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                LOGGER.debug("Server respond with 200 status to download controller with url " + getUrl());
                if (getPathToSave() == null) {
                    obtainFileNameFromHeader(connection);
                }
                LOGGER.debug("Path to file " + getPathToSave() + " in download controller with url " + getUrl());
                Files.deleteIfExists(getPathToSave());
                downloaded.set(0);
                download(connection);
            } else if (responseCode == HttpURLConnection.HTTP_PARTIAL) {
                //TODO : Add test for that case
                LOGGER.debug("Server respond with 206 status to download controller with url " + getUrl());
                download(connection);
            } else {
                throw new UnacceptableServerResponse("To download file server must response with status code 200. " +
                        "Actual response code: " + Integer.toString(responseCode));
            }
        } catch (InterruptedException | ClosedByInterruptException e) {
            synchronized (locker) {
                if (status == DownloadStatus.INTERRUPTED) {
                    handleInterruption(false);
                } else {
                    handleInterruption(true);
                }
            }
        } catch (OverlappingFileLockException e) {
            synchronized (locker) {
                if (status == DownloadStatus.INTERRUPTED) {
                    handleInterruption(Thread.currentThread().isInterrupted());
                } else {
                    handleError(e);
                }
            }
        } catch (IOException | SecurityException e) {
            handleError(e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        synchronized (locker) {
            if (status == DownloadStatus.RUN || status == DownloadStatus.RUN_AFTER_PAUSED) {
                status = DownloadStatus.FINISHED;
                LOGGER.debug("Download finished." + " URL " + getUrl());
            }
            notification.setStatus(status);
            return new DownloadResponse(status, reason, getPathToSave());
        }

    }

    private void download(final HttpURLConnection connection) throws IOException, InterruptedException {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(getPathToSave().toFile(), "rw");
             InputStream fileStream = connection.getInputStream();
             FileChannel fileChannel = randomAccessFile.getChannel();
             FileLock fileLock = fileChannel.tryLock()) {
            if (fileLock != null) {
                LOGGER.debug("Lock on file channel acquired on file " + getPathToSave() +
                        " in download controller with url " + getUrl());
                byte[] buffer = new byte[512];
                int count;
                while ((count = fileStream.read(buffer)) != -1) {
                    synchronized (locker) {
                        while (status == DownloadStatus.PAUSED) {
                            locker.wait();
                        }
                        if (Thread.currentThread().isInterrupted() || status == DownloadStatus.INTERRUPTED) {
                            throw new InterruptedException("Downloading was interrupted");
                        }
                    }
                    ByteBuffer byteBuffer = ByteBuffer.allocate(count);
                    byteBuffer.put(buffer, 0, count);
                    byteBuffer.flip();
                    fileChannel.write(byteBuffer);
                    downloaded.addAndGet(count);
                }
                LOGGER.debug("Process interrupted or file has been downloaded into " + getPathToSave() + " URL " + getUrl());
            } else {
                throw new UnableToLockFile("Unable to lock file. File is used somewhere outside" +
                        " i.e. to write file you have to close applications which is using this file");
            }
        } catch (SocketTimeoutException e) {
            boolean paused;
            synchronized (locker) {
                paused = (status == DownloadStatus.RUN_AFTER_PAUSED);
            }
            if (paused) {
                LOGGER.debug("Connection has been lost due to timeout and pausing. Reestablish connection");
                call();
            } else {
                throw e;
            }
        }
    }

    private void updateStatusToRun() {
        synchronized (locker) {
            if (status == DownloadStatus.NOT_STARTED || status == DownloadStatus.RUN_AFTER_PAUSED) {
                LOGGER.debug("initial status update in controller with url " +
                        getUrl() + ". Status RUN");
                status = DownloadStatus.RUN;
                notification.setStatus(status);
            } else {
                LOGGER.warn("initial status update failed in controller with url " +
                        getUrl() + ". Status " + status);
            }
        }
    }

    private HttpURLConnection initializeConnection() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) getUrl().openConnection();
        connection.setConnectTimeout(connectionTimeout);
        connection.setReadTimeout(readTimeout);
        connection.setRequestProperty("Range", "bytes=" + downloaded.get() + "-");
        return connection;
    }

    private void handleInterruption(boolean isThreadInterrupted) {

        LOGGER.warn("Download controller with url " + getUrl() + " was interrupted");
        if (isThreadInterrupted) {
            Thread.currentThread().interrupt();
        }
        synchronized (locker) {
            status = DownloadStatus.INTERRUPTED;
            notification.setStatus(status);
        }


    }

    private void handleError(Exception e) {
        LOGGER.error("Error in download controller with url " + getUrl() + " and path " + getPathToSave(), e);
        synchronized (locker) {
            status = DownloadStatus.FAILED;
            notification.setStatus(status);
            reason = e;
        }
    }

    private void obtainFileNameFromHeader(HttpURLConnection connection) {
        String fileName = "";
        String disposition = connection.getHeaderField("Content-Disposition");

        if (disposition != null) {
            // extracts file name from header field
            int index = disposition.indexOf("filename=");
            if (index > 0) {
                fileName = disposition.substring(index + 10,
                        disposition.length() - 1);
            }
        } else {
            // extracts file name from URL
            String urlString = getUrl().toString();
            fileName = urlString.substring(urlString.lastIndexOf("/") + 1,
                    urlString.length());
        }
        Path path = Paths.get(fileName);
        setPathToSave(path);

    }


}
