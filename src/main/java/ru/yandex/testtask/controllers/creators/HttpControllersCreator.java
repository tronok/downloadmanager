package ru.yandex.testtask.controllers.creators;

import ru.yandex.testtask.controllers.DownloadControllerInterface;
import ru.yandex.testtask.controllers.HttpDownloadController;
import ru.yandex.testtask.requests.HttpURIDownloadRequest;

import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Factory for producing collection of HttpDownloadController objects inside one of DownloadManagerInterface inheritance.
 * Class is thread-safe and objects of class can be used multiple times.
 *
 * @see ru.yandex.testtask.controllers.HttpDownloadController
 * @see ru.yandex.testtask.managers.DownloadManagerInterface
 */
public class HttpControllersCreator implements ControllersCreator {

    private final Collection<URL> urls;
    private final Collection<Path> paths;
    private final int connectionTimeout;
    private final int readTimeout;

    /**
     * @param request           request for downloading from http server without authorization.
     * @param connectionTimeout sets connect timeout for all URLs.
     * @param readTimeout       sets read timeout for all URLs
     */
    public HttpControllersCreator(HttpURIDownloadRequest request, int connectionTimeout, int readTimeout) {
        urls = request.getUrls();
        paths = request.getPaths();
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }

    public HttpControllersCreator(HttpURIDownloadRequest request) {
        this(request, 6000, 6000);
    }

    /**
     * Creates collection of HttpDownloadControllers classes.
     *
     * @return collection of DownloadController that have DownloadStatus equal to NOT_STARTED and set URL.
     * All elements are not null
     */
    @Override
    public Collection<DownloadControllerInterface> createControllers() {
        final LinkedList<DownloadControllerInterface> controllers = new LinkedList<>();
        Path curPath = null;
        Iterator<Path> pathIterator = null;
        if (paths != null) {
            pathIterator = paths.iterator();
        }

        for (URL url : urls) {
            if (pathIterator != null) {
                curPath = pathIterator.next();
            }
            controllers.add(new HttpDownloadController(url, curPath, connectionTimeout, readTimeout));
        }
        return controllers;
    }
}
