package ru.yandex.testtask.controllers.creators;

import ru.yandex.testtask.controllers.DownloadControllerInterface;

import java.util.Collection;

/**
 * Factory for producing collection of DownloadControllerInterface objects inside one of DownloadManagerInterface inheritance.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Factory_method_pattern">Factory method pattern</a>
 * @see ru.yandex.testtask.controllers.DownloadControllerInterface
 * @see ru.yandex.testtask.managers.DownloadManagerInterface
 */
public interface ControllersCreator {
    /**
     * Creates collection of inheritance from DownloadControllerInterface.
     * Method is considered to be called  multiple times and to be thread-safe.
     * Controllers are not required to be the same type.
     *
     * @return collection of DownloadController that have DownloadStatus equal to NOT_STARTED and set URL
     * @see ru.yandex.testtask.utilities.DownloadStatus
     */
    public Collection<DownloadControllerInterface> createControllers();
}
