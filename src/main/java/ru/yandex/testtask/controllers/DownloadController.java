package ru.yandex.testtask.controllers;

import ru.yandex.testtask.utilities.DownloadStatus;

import java.net.URL;
import java.nio.file.Path;

/**
 * Helper class for simplification of DownloadController implementation. It provides public getters url and path to save
 * and attempts to hide setters in inheritance.
 */
public abstract class DownloadController implements DownloadControllerInterface {
    protected DownloadStatus status;
    private URL url;
    private Path pathToSave;

    public Path getPathToSave() {
        return pathToSave;
    }

    protected void setPathToSave(Path pathToSave) {
        this.pathToSave = pathToSave;
    }

    public URL getUrl() {
        return url;
    }

    protected void setUrl(URL url) {
        this.url = url;
    }

    /**
     * Contains current downloading status and URL. Provides ability to await for status change. Status is updated from within
     * one of DownloadControllerInterface inheritance. All operation with status is synchronized, awaiting is blocking.
     *
     * @see ru.yandex.testtask.controllers.DownloadControllerInterface
     */
    public class StatusNotificationRealization implements StatusNotification {
        private final Object locker;
        private final URL url;
        private DownloadStatus status;
        private volatile Path pathToSave;
        private volatile Throwable reason;
        private boolean changed;

        /**
         * Initial status and URL
         *
         * @param status initial status.
         * @param url    url of downloading.
         * @throws java.lang.NullPointerException thrown if status or URL is null.
         */
        public StatusNotificationRealization(DownloadStatus status, URL url) {
            if (status == null) {
                throw new NullPointerException("status should not be null");
            }
            if (url == null) {
                throw new NullPointerException("url should not be null");
            }
            this.status = status;
            changed = false;
            this.url = url;
            locker = new Object();
        }

        public DownloadStatus getStatus() {
            synchronized (locker) {
                return status;
            }
        }


        /**
         * Updates status in case when it changed. Waiting in awaitStatusChange will be notified.
         *
         * @param newStatus status of downloading.
         */
        public void setStatus(DownloadStatus newStatus) {
            synchronized (locker) {

                if (this.status != newStatus) {
                    changed = true;
                    this.status = newStatus;
                    locker.notifyAll();
                }
            }
        }


        public Path getPathToSave() {
            return pathToSave;
        }

        private void setPathToSave(Path pathToSave) {


            this.pathToSave = pathToSave;
        }

        public Throwable getReason() {
            return reason;
        }

        private void setReason(Throwable reason) {

            this.reason = reason;
        }

        public URL getUrl() {
            return url;
        }

        /**
         * Waits till status changing.
         *
         * @throws InterruptedException thrown if thread was interrupted during waiting.
         */
        public void awaitStatusChange() throws InterruptedException {
            synchronized (locker) {
                while (!changed) {
                    locker.wait();
                }
                changed = false;
            }
        }


    }
}
