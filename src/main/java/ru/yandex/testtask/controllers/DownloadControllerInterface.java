package ru.yandex.testtask.controllers;

import ru.yandex.testtask.responses.DownloadResponse;
import ru.yandex.testtask.utilities.DownloadStatus;

import java.net.URL;
import java.nio.file.Path;
import java.util.concurrent.Callable;

/**
 * Provides functionality to download files.  Downloading will be run by call method of Callable.
 * Any inheritance is considered to provide ability to pause, cancel and
 * stop task or at least mock it. To find exact meaning of mocked and real ability see description of method.
 * <p>
 * No restrictions on protocol are considered in inheritances.
 * Note that thread downloading can be interrupted and in that case you have to change status in both StatusNotification
 * and  DownloadResponse object to DownloadStatus.INTERRUPTED. NotificationStatus object have to be the same object among all returns
 * </p>
 *
 * @see java.util.concurrent.Callable
 * @see ru.yandex.testtask.responses.DownloadResponse
 */
public interface DownloadControllerInterface extends Callable<DownloadResponse> {
    /**
     * Gets path where file will be saved.
     *
     * @return Path where file will be saved.
     */
    public Path getPathToSave();

    /**
     * Gets url of target of download.
     *
     * @return url of target of download.
     */
    public URL getUrl();

    /**
     * <p>
     * Pauses downloading. Method should not make any change if current status of downloading is DownloadStatus.INTERRUPTED,
     * DownloadStatus.FINISHED or DownloadStatus.FAILED.
     * </p>
     * <p/>
     * If protocol cannot do pausing or by any other reason it's impossible to fairly pause downloading,
     * you can mock by simple change DownloadStatus of StatusNotification to PAUSED and getStatus
     * should return DownloadResponse with DownloadStatus.PAUSED status. In the case when real ability to pause can be provided, method
     * also should stop downloading ASAP but keeps all made progress and can be resumed after call resume method.
     *
     * @return notification object with status changed to PAUSED.
     * @see ru.yandex.testtask.utilities.DownloadStatus
     */
    public StatusNotification pause();

    /**
     * <p>
     * Resumes downloading. Nothing should happen if downloading was not paused.
     * </p>
     * <p/>
     * If protocol cannot do resuming or by any other reason it's impossible to
     * fairly resume downloading, you can mock by simple change DownloadStatus of StatusNotification to DownloadStatus.RUN and getStatus
     * should return DownloadResponse with DownloadStatus.RUN status. In the case when real ability to pause can be provided, method
     * also should resume downloading ASAP.
     *
     * @return notification object with status changed to DownloadStatus.RUN if previous state was  DownloadStatus.PAUSED.
     */
    public StatusNotification resume();

    /**
     * <p>
     * Cancel downloading. Nothing should happen if downloading finished or failed.
     * </p>
     * <p/>
     * If protocol cannot do canceling or by any other reason it's impossible to
     * interrupt downloading, you can mock by simple change DownloadStatus of StatusNotification to DownloadStatus.INTERRUPTED and getStatus
     * should return DownloadResponse with DownloadStatus.INTERRUPTED status. In the case when real ability to interrupt can be provided, method
     * also should interrupt downloading ASAP.
     *
     * @return notification object with status changed to DownloadStatus.INTERRUPTED if previous state was any but
     * DownloadStatus.FINISHED or DownloadStatus.FAILED.
     */
    public StatusNotification cancel();


    /**
     * Returns notification token of this downloading. Notification is considered to be held inside controller.
     *
     * @return notification token that is not null.
     */
    public StatusNotification getNotificationToken();


    /**
     * Contains current downloading status and URL. Provides ability to await for status change. Status is updated from within
     * one of DownloadControllerInterface inheritance. All operation with status is synchronized, awaiting is blocking.
     */
    public interface StatusNotification {
        public DownloadStatus getStatus();


        public Path getPathToSave();

        public Throwable getReason();


        public URL getUrl();

        /**
         * Waits till status changing.
         *
         * @throws InterruptedException thrown if thread was interrupted during waiting.
         */
        public void awaitStatusChange() throws InterruptedException;
    }
}
