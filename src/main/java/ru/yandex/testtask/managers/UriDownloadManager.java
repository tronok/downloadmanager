package ru.yandex.testtask.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.testtask.controllers.DownloadControllerInterface;
import ru.yandex.testtask.controllers.DownloadControllerInterface.StatusNotification;
import ru.yandex.testtask.controllers.creators.ControllersCreator;
import ru.yandex.testtask.exceptions.UnknownDownloadingException;
import ru.yandex.testtask.requests.DownloadRequestInterface;
import ru.yandex.testtask.responses.DownloadResponse;
import ru.yandex.testtask.utilities.DownloadStatus;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;

/**
 * DownloadManager where all URLs should be representable as URIs. It's thread-safe singleton.
 */
public final class UriDownloadManager implements DownloadManagerInterface {
    private final static Logger LOGGER = LoggerFactory.getLogger(UriDownloadManager.class);
    private final static UriDownloadManager ourInstance = new UriDownloadManager();
    private final Object lockerForDownloading;
    private final Object lockerForClearing;
    private final ConcurrentHashMap<URI, ControlToken> downloads;
    private final ExecutorService executor;
    private final ScheduledExecutorService scheduledExecutor;
    private ScheduledFuture<?> autoCleaner;


    private UriDownloadManager() {
        downloads = new ConcurrentHashMap<>();
        executor = Executors.newCachedThreadPool();
        lockerForDownloading = new Object();
        scheduledExecutor = Executors.newScheduledThreadPool(1);
        lockerForClearing = new Object();
    }

    public static UriDownloadManager getInstance() {
        return ourInstance;
    }

    /**
     * Adds request for downloading.
     *
     * @param request request for downloading
     * @return hash map where keys are URIs obtained from URLs and values are futures of downloading.
     * If URL can not be represented as URI or error happen during method work, such URL will not be placed into this map.
     * If URL is downloaded right now than value (Future) of such request will be null.
     */
    @Override
    public HashMap<URI, Future<DownloadResponse>> download(DownloadRequestInterface request) {
        Collection<DownloadControllerInterface> controllers = request.getCreator().createControllers();
        HashMap<URI, Future<DownloadResponse>> futures = new HashMap<>(controllers.size());
        for (DownloadControllerInterface controller : controllers) {
            synchronized (lockerForDownloading) {

                try {
                    URL url = controller.getUrl();
                    URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                            url.getPath(), url.getQuery(), url.getRef());
                    if (!downloads.containsKey(uri)) {

                        Future<DownloadResponse> future = executor.submit(controller);
                        downloads.put(uri, new ControlToken(controller, future, request.getCreator()));
                        futures.put(uri, future);
                        LOGGER.debug("URI " + uri + " has been added to download");

                    } else {
                        LOGGER.debug("URL " + controller.getUrl() + " is downloading. " +
                                "Stop and restart instead of additional running");
                        futures.put(uri, null);
                    }
                } catch (URISyntaxException e) {
                    LOGGER.error("URL " + controller.getUrl() + " cannot be represented as URI", e);
                } catch (NullPointerException e) {
                    LOGGER.error("Critical exception.", e);

                }

            }
        }
        return futures;
    }

    /**
     * Tells downloading controller to pause.
     *
     * @param id URL to find download
     * @throws URISyntaxException thrown if URL can not be represented as URI
     */
    @Override
    public void pause(URL id) throws URISyntaxException {
        URI uri = new URI(id.getProtocol(), id.getUserInfo(), id.getHost(), id.getPort(),
                id.getPath(), id.getQuery(), id.getRef());
        pause(uri);
    }

    /**
     * Tells downloading controller to pause.
     *
     * @param id URI to find download
     */
    public void pause(URI id) {
        ControlToken controlToken = downloads.get(id);
        if (controlToken == null) {
            LOGGER.warn("URI " + id + " has been already deleted");
            throw new UnknownDownloadingException("Queried element (URI:" + id + ") has been already deleted or has ever been added");
        }
        controlToken.controller.pause();
        LOGGER.debug("URI " + id + " download was paused");
    }

    /**
     * Tells downloading controller to resume.
     *
     * @param id URL to find download
     * @throws URISyntaxException thrown if URL can not be represented as URI
     */
    @Override
    public void resume(URL id) throws URISyntaxException {
        URI uri = new URI(id.getProtocol(), id.getUserInfo(), id.getHost(), id.getPort(),
                id.getPath(), id.getQuery(), id.getRef());
        resume(uri);
    }

    /**
     * Tells downloading controller to resume.
     *
     * @param id URI to find download
     */
    public void resume(URI id) {
        ControlToken controlToken = downloads.get(id);
        if (controlToken == null) {
            LOGGER.warn("URI " + id + " has been already deleted");
            throw new UnknownDownloadingException("Queried element (URI:" + id + ") has been already deleted or has ever been added");
        }
        controlToken.controller.resume();
        LOGGER.debug("URI " + id + " download was resumed");
    }

    /**
     * Attempts cancel of downloading by thread interruption.
     *
     * @param id URL to find download
     * @throws URISyntaxException thrown if URL can not be represented as URI
     */
    @Override
    public boolean cancel(URL id) throws URISyntaxException {
        URI uri = new URI(id.getProtocol(), id.getUserInfo(), id.getHost(), id.getPort(),
                id.getPath(), id.getQuery(), id.getRef());
        return cancel(uri);
    }

    /**
     * Attempts cancel of downloading by thread interruption.
     *
     * @param id URI to find download
     */
    public boolean cancel(URI id) {
        ControlToken controlToken = downloads.get(id);
        if (controlToken == null) {
            LOGGER.warn("URI " + id + " has been already deleted");
            throw new UnknownDownloadingException("Queried element (URI:" + id + ") has been already deleted or has ever been added");
        }
        boolean canBeCanceled = controlToken.future.cancel(true);
        if (canBeCanceled) {
            LOGGER.debug("URI " + id + " download was canceled");
        } else {
            LOGGER.warn("Unable to cancel download with URI " + id);
        }
        return canBeCanceled;
    }

    /**
     * Stops uploading by controller's cancel method and attempts to restart - delete and start new download.
     *
     * @param id URL to find download
     * @return First future will complete when download restart. The second one is future for downloading
     * (the same can be obtained from getFuture method).
     * @throws URISyntaxException thrown if URL can not be represented as URI
     */
    @Override
    public Future<Future<DownloadResponse>> stopAndRestart(URL id) throws URISyntaxException {
        URI uri = new URI(id.getProtocol(), id.getUserInfo(), id.getHost(), id.getPort(),
                id.getPath(), id.getQuery(), id.getRef());
        return stopAndRestart(uri);
    }

    /**
     * Stops uploading by controller's cancel method and attempts to restart - delete and start new download.
     *
     * @param id URL to find download
     * @return First future will complete when download restart. The second one is future for downloading
     * (the same can be obtained from getFuture method).
     */
    public Future<Future<DownloadResponse>> stopAndRestart(URI id) {
        return stopAndDeleteInternal(id, true);
    }

    /**
     * Stops uploading by controller's cancel method and delete downloads.
     *
     * @param id URL to find download
     * @throws URISyntaxException thrown if URL can not be represented as URI
     */
    @Override
    public void stopAndDelete(URL id) throws URISyntaxException {
        URI uri = new URI(id.getProtocol(), id.getUserInfo(), id.getHost(), id.getPort(),
                id.getPath(), id.getQuery(), id.getRef());
        stopAndDelete(uri);
    }

    public void stopAndDelete(URI id) {
        stopAndDeleteInternal(id, false);
    }

    private Future<Future<DownloadResponse>> stopAndDeleteInternal(URI id, boolean restart) {
        ControlToken controlToken = downloads.get(id);
        if (controlToken == null) {
            LOGGER.warn("URI " + id + " has been already deleted");
            throw new UnknownDownloadingException("Queried element (URI:" + id + ") has been already deleted or has ever been added");
        }
        Future<Future<DownloadResponse>> future = null;
        LOGGER.debug("URI " + id + " was canceled in deletion");
        try {
            future = executor.submit(new DownloadDestructor(controlToken, restart));
        } catch (URISyntaxException e) {
            LOGGER.error("Unable to delete download with id " + controlToken.controller.getUrl() + "" +
                    " due to exception. ", e);
        }
        return future;

    }


    /**
     * Gets future object for required URL
     *
     * @param id URL to find download
     * @return future object for downloading process
     * @throws URISyntaxException thrown if URL can not be represented as URI
     */
    @Override
    public Future<DownloadResponse> getFuture(URL id) throws URISyntaxException {
        URI uri = new URI(id.getProtocol(), id.getUserInfo(), id.getHost(), id.getPort(),
                id.getPath(), id.getQuery(), id.getRef());
        return getFuture(uri);
    }

    /**
     * Gets future object for required URL
     *
     * @param id URL to find download
     * @return future object for downloading process
     */
    public Future<DownloadResponse> getFuture(URI id) {
        ControlToken controlToken = downloads.get(id);
        if (controlToken == null) {
            LOGGER.warn("URI " + id + " has been already deleted");
            throw new UnknownDownloadingException("Queried element (URI:" + id + ") has been already deleted or has ever been added");
        }
        return controlToken.future;
    }

    /**
     * Gets notification for required URL.
     *
     * @param id URL to find download
     * @return notification for required URL
     * @throws URISyntaxException thrown if URL can not be represented as URI
     */
    @Override
    public StatusNotification getNotification(URL id) throws URISyntaxException {
        URI uri = new URI(id.getProtocol(), id.getUserInfo(), id.getHost(), id.getPort(),
                id.getPath(), id.getQuery(), id.getRef());
        return getNotification(uri);
    }

    /**
     * Clears all downloads that have status finished.
     *
     * @return list of removed downloads.
     * @throws RuntimeException thrown if URI cannot be converted in URL.
     */
    @Override
    public List<URL> clearFinishedDownloads() {
        Set<Map.Entry<URI, ControlToken>> toClear = downloads.entrySet();
        List<URL> cleared = new LinkedList<>();
        for (Map.Entry<URI, ControlToken> tokenEntry : toClear) {
            if (tokenEntry.getValue().controller.getNotificationToken().getStatus() == DownloadStatus.FINISHED) {
                if (downloads.remove(tokenEntry.getKey()) != null) {
                    try {
                        LOGGER.warn(tokenEntry.getKey() + " deleted");
                        cleared.add(tokenEntry.getKey().toURL());
                    } catch (MalformedURLException e) {
                        LOGGER.error("CRITICAL ERROR. Unable to convert URI back to URL");
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return cleared;
    }


    /**
     * Stops and deletes all already submitted downloads in background. It's non-blocking operation.
     * Submission of new downloads will not be prevented.
     */
    @Override
    public void shutdownAllDownloads() {
        Set<Map.Entry<URI, ControlToken>> toClear = downloads.entrySet();
        for (Map.Entry<URI, ControlToken> tokenEntry : toClear) {
            stopAndDelete(tokenEntry.getKey());
        }
    }


    /**
     * Disables further clearing. If current cleaner works it will NOT be canceled. In case when auto clearing was
     * not enabled nothing will happen.
     */
    public void disableAutoClearingFinished() {
        synchronized (lockerForClearing) {
            if (autoCleaner != null) {
                autoCleaner.cancel(false);
                autoCleaner = null;
            }
        }

    }

    /**
     * Enables auto clearing of finished downloads. If it was previously enabled nothing will happen.
     *
     * @param initialDelay the time to delay first execution of clearing.
     *                     If it less or equal to zero clearing will start immediately.
     * @param period       the period between successive executions. Should be greater than zero.
     * @param unit         the time unit of the initialDelay and period parameters
     * @throws IllegalArgumentException if period less than or equal to zero
     */
    public void enableAutoClearingFinished(long initialDelay, long period, TimeUnit unit) {
        synchronized (lockerForClearing) {
            if (autoCleaner == null) {
                autoCleaner = scheduledExecutor.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        clearFinishedDownloads();
                    }
                }, initialDelay, period, unit);
            }
        }
    }

    /**
     * Gets notification for required URI.
     *
     * @param id URI to find download
     * @return notification for required URI
     */
    public StatusNotification getNotification(URI id) {
        ControlToken controlToken = downloads.get(id);
        if (controlToken == null) {
            LOGGER.warn("URI " + id + " has been already deleted");
            throw new UnknownDownloadingException("Queried element (URI:" + id + ") has been already deleted or has ever been added");
        }
        return controlToken.controller.getNotificationToken();
    }


    private static class ControlToken {
        final ControllersCreator creator;
        Future<DownloadResponse> future;
        DownloadControllerInterface controller;

        ControlToken(DownloadControllerInterface controller, Future<DownloadResponse> future,
                     ControllersCreator creator) {
            this.controller = controller;
            this.future = future;
            this.creator = creator;
        }
    }

    /**
     * For stopping, deletion and (if required) restarting of download.
     */
    private class DownloadDestructor implements Callable<Future<DownloadResponse>> {
        private final Logger LOGGER = LoggerFactory.getLogger(getClass());
        private final ControlToken controlToken;
        private final URI id;
        private final boolean restart;

        DownloadDestructor(ControlToken controlToken, boolean restart) throws URISyntaxException {
            this.controlToken = controlToken;
            URL url = controlToken.controller.getUrl();
            id = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                    url.getPath(), url.getQuery(), url.getRef());
            this.restart = restart;
        }

        private Future<DownloadResponse> restartDownload() {
            Collection<DownloadControllerInterface> controllers = controlToken.creator.createControllers();
            DownloadControllerInterface required = null;
            //Finds required controller with specific URL.
            for (DownloadControllerInterface controller : controllers) {
                try {
                    URL url = controller.getUrl();
                    URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                            url.getPath(), url.getQuery(), url.getRef());
                    if (controller.getPathToSave().equals(controlToken.controller.getPathToSave()) && uri.equals(id)) {
                        required = controller;
                        break;
                    }
                } catch (URISyntaxException e) {
                    LOGGER.error("Controller's URL" + controller.getUrl() + " cannot be represented as URI", e);
                }
            }
            if (required == null) {
                LOGGER.error("Critical error. Required controller has not been found. URI " + id);
                return null;
            }
            Future<DownloadResponse> future = null;
            if (!downloads.containsKey(id)) {
                future = executor.submit(required);
                controlToken.controller = required;
                controlToken.future = future;
                downloads.put(id, controlToken);
                LOGGER.debug("URI " + id + " has been added to download");
            } else {
                LOGGER.debug("URI " + id + " is downloading. " +
                        "Stop and restart instead of additional running");
            }
            LOGGER.debug("URI " + id + " was restarted");
            return future;
        }

        @Override
        public Future<DownloadResponse> call() {
            StatusNotification notification = controlToken.controller.cancel();
            DownloadStatus status = notification.getStatus();
            try {
                LOGGER.debug("Status in deletion: " + status + " URI " + id);
                while (status != DownloadStatus.FAILED && status != DownloadStatus.FINISHED &&
                        status != DownloadStatus.INTERRUPTED) {
                    notification.awaitStatusChange();
                    status = notification.getStatus();
                    LOGGER.debug("Status changed. New status: " + status + ". URI " + id);

                }
            } catch (InterruptedException e) {
                LOGGER.error("Unable to delete download with URI " + id + " due to exception. ", e);
            }
            LOGGER.debug("URI " + id + " was cancelled");
            downloads.remove(id);
            LOGGER.debug("URI " + id + " was removed");
            if (!restart) {
                return null;
            }
            return restartDownload();
        }
    }


}


