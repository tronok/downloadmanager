package ru.yandex.testtask.managers;

import ru.yandex.testtask.controllers.DownloadControllerInterface;
import ru.yandex.testtask.requests.DownloadRequestInterface;
import ru.yandex.testtask.responses.DownloadResponse;

import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.Future;

/**
 * Describes basic functionality of download manager. It provides ability to download data using DownloadRequestInterface
 * and full control over downloads. Nevertheless such methods as pause, resume, cancel depend on specific for downloading
 * DownloadControllerInterface realization. stopAndDelete, stopAndRestart partially depend.
 *
 * @see ru.yandex.testtask.requests.DownloadRequestInterface
 */
public interface DownloadManagerInterface {
    /**
     * Adds request to download. It's not guarantee that for all URLs downloading will  be started.
     *
     * @param request request for downloading
     * @return hash map with information about downloads that were added. Key and values depend on realization.
     */
    public HashMap download(DownloadRequestInterface request);

    /**
     * Tells downloading controller to pause.
     *
     * @param id URL to find download
     * @throws Exception
     */
    public void pause(URL id) throws Exception;

    /**
     * Tells downloading controller to resume.
     *
     * @param id URL to find download
     * @throws Exception
     */
    public void resume(URL id) throws Exception;

    /**
     * Attempts cancel of downloading by thread interruption or by controller cancel method.
     *
     * @param id URL to find download
     * @throws Exception
     */
    public boolean cancel(URL id) throws Exception;

    /**
     * Stops uploading (for instance, by canceling it) and attempts to restart - delete and start new download.
     *
     * @param id URL to find download
     * @return First future will complete when download restart. The second one is future for downloading
     * (the same can be obtained from getFuture method).
     * @throws Exception
     */
    public Future<Future<DownloadResponse>> stopAndRestart(URL id) throws Exception;

    /**
     * Stops uploading (for instance, by canceling it) and delete downloads.
     *
     * @param id URL to find download
     * @throws Exception
     */
    public void stopAndDelete(URL id) throws Exception;


    /**
     * Gets future object for required URL
     *
     * @param id URL to find download
     * @return future object for downloading process
     * @throws Exception
     */
    public Future<DownloadResponse> getFuture(URL id) throws Exception;

    /**
     * Gets notification for required URL.
     *
     * @param id URL to find download
     * @return notification for required URL
     * @throws Exception
     */
    public DownloadControllerInterface.StatusNotification getNotification(URL id) throws Exception;

    /**
     * Clears all downloads that have status finished.
     *
     * @return list of removed downloads.
     */
    public Collection<URL> clearFinishedDownloads();


    /**
     * Stops and deletes all already submitted downloads.
     */
    public void shutdownAllDownloads();
}
