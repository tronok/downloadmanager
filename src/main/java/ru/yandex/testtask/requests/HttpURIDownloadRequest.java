package ru.yandex.testtask.requests;

import ru.yandex.testtask.controllers.creators.ControllersCreator;
import ru.yandex.testtask.controllers.creators.HttpControllersCreator;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;

/**
 * Request for downloading over HTTP without authorization. Produces HttpControllersCreator as ControllersCreator
 *
 * @see ru.yandex.testtask.controllers.creators.ControllersCreator
 * @see ru.yandex.testtask.controllers.creators.HttpControllersCreator
 * @see java.net.URI
 * @see java.net.URL
 */
public class HttpURIDownloadRequest extends DownloadRequest {

    /**
     * Create http request with URLs that can be represent as URI.
     *
     * @param urls  collection of URLs
     * @param paths collection of paths where data will be saved.
     * @throws URISyntaxException thrown if one of URL cannot be represented as URI.
     */
    public HttpURIDownloadRequest(Collection<URL> urls, Collection<Path> paths) throws URISyntaxException {
        setUrls(urls);
        for (URL url : urls) {
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                    url.getPath(), url.getQuery(), url.getRef());
        }
        setPaths(paths);
    }

    /**
     * Creates new HttpControllersCreator with itself as passed into creator request.
     *
     * @return HttpControllersCreator as ControllersCreator
     */
    @Override
    public ControllersCreator getCreator() {
        return new HttpControllersCreator(this);
    }
}
