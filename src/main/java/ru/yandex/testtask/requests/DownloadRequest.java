package ru.yandex.testtask.requests;

import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;

/**
 * Class-helper for realization of DownloadRequestInterface. It provides public getters urls and paths to save
 * and attempts to hide setters in inheritance. Setters controls conformity of size between urls and paths and make nulls checking
 */
public abstract class DownloadRequest implements DownloadRequestInterface {
    private Collection<URL> urls;
    private Collection<Path> paths;

    /**
     * Gets collection of urls. Cannot be null and cannot contain null
     * @return collection of urls.
     */
    public Collection<URL> getUrls() {
        return urls;
    }

    /**
     * Sets URLs. Checks URLs collection on null and tests size equality of paths and URLs.
     * @param urls collection of URLs
     * @throws java.lang.NullPointerException if collection of URLs is null or any URL is null
     * @throws java.lang.IllegalArgumentException paths are not null and sizes of urls and paths are not equal.
     */
    protected void setUrls(Collection<URL> urls) {
        if (urls == null) {
            throw new NullPointerException("URLs collection must be not null.");
        }
        if (paths != null && urls.size() != paths.size()) {
            throw new IllegalArgumentException("Size of urls collection must be equal to size of paths collection " +
                    "unless paths are null. Urls size: " + Integer.toString(urls.size()) + ". Paths size: " +
                    Integer.toString(paths.size()));
        }
        for (URL url : urls) {
            if (url == null) {
                throw new NullPointerException("Urls should be not null");
            }
        }
        this.urls = urls;
    }

    /**
     * Gets collection of paths. Can be null and can contain null.
     * @return collection of paths.
     */
    public Collection<Path> getPaths() {
        return paths;
    }

    /**
     * Sets paths Tests size equality of paths and URLs.
     *
     * @param paths collection of path to file for downloading.
     * @throws java.lang.IllegalArgumentException paths are not null and urls are not null and sizes of urls and paths are not equal.
     */
    protected void setPaths(Collection<Path> paths) {
        if (paths != null && urls != null && urls.size() != paths.size()) {
            throw new IllegalArgumentException("Size of paths collection must be equal to size of url collection " +
                    "unless paths are null. Urls size: " + Integer.toString(urls.size()) + ". Paths size: " +
                    Integer.toString(paths.size()));
        }
        this.paths = paths;
    }
}
