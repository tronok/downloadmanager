package ru.yandex.testtask.requests;

import ru.yandex.testtask.controllers.creators.ControllersCreator;

import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;

/**
 * It provides the most general description of download parameters. Also it can create specific for such request
 * ControllersCreator in the thread-safe way.
 * Interface for usage in ControllersCreator inheritance and  DownloadManagerInterface inheritance.
 *
 * @see ru.yandex.testtask.controllers.creators.ControllersCreator
 * @see ru.yandex.testtask.managers.DownloadManagerInterface
 */
public interface DownloadRequestInterface {
    /**
     * Gets collection of urls to download in the thread-safe way.
     *
     * @return collection of urls to download
     */
    public Collection<URL> getUrls();

    /**
     * Gets collection of paths where downloading will be saved. Method is considered to work in the thread-safe way.
     *
     * @return collection of paths where downloading will be saved.
     */
    public Collection<Path> getPaths();

    /**
     * Creates ControllersCreator this specific request.
     *
     * @return ControllersCreator instance
     * @see ControllersCreator
     */
    public ControllersCreator getCreator();
}
