package ru.yandex.testtask.exceptions;

import java.io.IOException;

/**
 * UnableToLockFile is thrown in case of inability to acquire lock on file for downloading.
 */
public class UnableToLockFile extends IOException {
    public UnableToLockFile() {
    }

    public UnableToLockFile(String message) {
        super(message);
    }

    public UnableToLockFile(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToLockFile(Throwable cause) {
        super(cause);
    }
}
