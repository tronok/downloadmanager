package ru.yandex.testtask.exceptions;

import java.io.IOException;

/**
 * UnacceptableServerResponse is thrown when server response with any other response code but 200
 */
public class UnacceptableServerResponse extends IOException {
    public UnacceptableServerResponse() {
    }

    public UnacceptableServerResponse(String message) {
        super(message);
    }

    public UnacceptableServerResponse(String message, Throwable cause) {
        super(message, cause);
    }

    public UnacceptableServerResponse(Throwable cause) {
        super(cause);
    }
}
