package ru.yandex.testtask.exceptions;

/**
 * UnknownDownloadingException is thrown when UriDownloadManager cannot find requested downloading.
 * @see ru.yandex.testtask.managers.UriDownloadManager
 */
public class UnknownDownloadingException extends IllegalStateException {
    public UnknownDownloadingException() {
    }

    public UnknownDownloadingException(String s) {
        super(s);
    }

    public UnknownDownloadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownDownloadingException(Throwable cause) {
        super(cause);
    }
}
