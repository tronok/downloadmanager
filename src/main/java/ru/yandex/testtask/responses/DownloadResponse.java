package ru.yandex.testtask.responses;

import ru.yandex.testtask.utilities.DownloadStatus;

import java.nio.file.Path;

/**
 * Information about downloading. Contains status, exception (if was thrown) and path where file will be saved.
 * @see ru.yandex.testtask.utilities.DownloadStatus
 */
public class DownloadResponse {
    private final DownloadStatus status;
    private final Throwable reasonOfFail;
    private final Path pathToFile;

    /**
     * Creates DownloadResponse with all parameters.
     *
     * @param status       current status of downloading. Can not be null
     * @param reasonOfFail exception that caused fail (if it was) of downloading
     * @param pathToFile   path where data will be/was saved.
     * @throws java.lang.NullPointerException if status is null
     */
    public DownloadResponse(DownloadStatus status, Throwable reasonOfFail, Path pathToFile) {
        if (status == null) {
            throw new NullPointerException("Status must be provided");
        }
        this.status = status;
        this.pathToFile = pathToFile;
        this.reasonOfFail = reasonOfFail;
    }

    public DownloadStatus getStatus() {
        return status;
    }

    public Throwable getReasonOfFail() {
        return reasonOfFail;
    }


    public Path getPathToFile() {
        return pathToFile;
    }

    /**
     * Tests equality of two DownloadResponse using all fields with public setters.
     * @param o object to compare
     * @return true if status, exception and path are equal to o.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DownloadResponse)) return false;

        DownloadResponse response = (DownloadResponse) o;

        if (pathToFile != null ? !pathToFile.equals(response.pathToFile) : response.pathToFile != null) return false;
        if (reasonOfFail != null ? !reasonOfFail.equals(response.reasonOfFail) : response.reasonOfFail != null)
            return false;
        return status == response.status;

    }

    @Override
    public int hashCode() {
        int result = status.hashCode();
        result = 31 * result + (reasonOfFail != null ? reasonOfFail.hashCode() : 0);
        result = 31 * result + (pathToFile != null ? pathToFile.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DownloadResponse{" +
                "status=" + status +
                ", reasonOfFail=" + reasonOfFail +
                ", pathToFile=" + pathToFile +
                '}';
    }
}
